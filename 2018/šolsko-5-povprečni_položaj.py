
def štej_znake(znak):
	ch_besedilo = []
	do_zdaj = 0

	with open("šolsko-5-povprečni_položaj.input") as f:
		for i, vrstica in enumerate(f):
			ch_vrstica = []

			for poz, ch in enumerate(vrstica):
				if ch == znak:
					ch_vrstica.append(poz)
					ch_besedilo.append(do_zdaj + poz)

			print(f"Vrstica {i}: pojavitve={len(ch_vrstica)} povprečje={sum(ch_vrstica) // len(ch_vrstica) + 1 if ch_vrstica else 0}")
			do_zdaj += len(vrstica)
	print(f"Celotno besedilo: pojavitve={len(ch_besedilo)} povprečje={sum(ch_besedilo) // len(ch_besedilo) + 1}")

# Pravilen rezultat:
# 	Povprečja: [0, 49, 25, 34, 12] ✔
# 	V celotnem besedilu: 31 ✘ (TODO: )
štej_znake("k")
