"""
Preizkusi vse k med a in b. 
	Če je vrednost večja od trenutnega maksimuma, ga shrani. Seznam enakih resetira in doda k.
	Če je vrednost enaka trenutnemu maksimumu, k doda na seznam enakih.
"""


def collatz(k: int):
	n = k
	res = [n]
	
	while n != 1:
		if n % 2 == 0:
			n = n/2
		else:
			n = 3*n + 1
		res.append(n)
	return res


def PoisciVse(a: int, b: int):
	maxx = 0
	res = []
	# Za vsako št. med a in b
	for k in range(a, b+1):
		# Poišči maksimum
		m = max(collatz(k))
		
		# Če je večji, resetiraj rezultat
		if m > maxx:
			res = []
			maxx = m
		
		# Če je večji ali enak, dodaj k med rezultate
		if m >= maxx:
			res.append(k)
	
	# Vrni rezultat
	return res


print(PoisciVse(30, 50))  # Pravilen rezultat: [31, 41, 47] ✔
