"""
Program poišče vse delitelje števila koščkov in jih smatra kot višino.
	Ker je edini delitelj nad polovico števila (n) število samo, gremo le do n/2.
	Ker iščemo le razmerja, kjer
	Če je ostanek pri deljenju s številom enak 0, je število delitelj.
	Izračuna razmerje med višino in širino, ter ga primerja s podanim.
	Če je razmerje bližje podanemu kot prejšnje znano, ga zamenja.
"""


def Sestavljanka(steviloKosckov: int, razmerje: float) -> (int, int):
	# Začni edinim razmerjem, ki sigurno obstaja
	# Spodnja zanka sicer pride do te vrednosti, 
	višina, širina = 1, steviloKosckov
	diff = abs(razmerje - višina/širina)
	
	for h in range(1, round(steviloKosckov/2)):
		# Če je število deljitelj
		if steviloKosckov % h == 0:
			# Izračunaj višino in razmerje
			w = round(steviloKosckov / h)
			r = h / w
			# Izračunaj razliko
			d = abs(razmerje - r)
			# Če je razlika manjša, jo shrani
			if d < diff:
				diff = d
				višina, širina = h, w
	
	# Vrni dimenzije sestavljanke
	return višina, širina


print(Sestavljanka(136, 0.428))  # Pravilen rezultat: (8, 17) ✔
