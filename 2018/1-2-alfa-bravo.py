"""
Kopirana tabela iz spletne učilnice je bila pretvorjena v Python seznam s pomočjo RegEx zamenjave v Notepad++.
Program najprej prebrano besedilo razdeli po presledkih (na besede).
Za vsako besede gre skozi seznam kodnih besed in poišče tisto, s katero se ujema (funkcija close_enough).
Iz najdene kodne besede prvo črko prepiše v rezultatni niz, ki ga na koncu izpiše
"""


def close_enough(a: str, b: str) -> bool:
	"""Preveri, če se niza razlikujeta za manj kot 1 znak"""
	# Če se dolžina besed ne ujema, si nista dovolj podobni
	if len(a) != len(b):
		return False
	
	napake = 0
	for i in range(0, len(a)):
		# Če se istoležni črki ne ujemata, prištej eno napako
		if a[i] != b[i]:
			napake += 1
		# Če je napak več, si nista dovolj podobni
		if napake > 1:
			return False
	# Če prideš do sem, sta si besedi dovolj podobni
	return True


kode = ["ALFA", "BRAVO", "CHARLIE", "DELTA", "ECHON", "FOXTROT", "GOLF", "HOTEL", 
		"INDIA", "JULIET", "KILO", "LIMA", "MIKE", "NOVEMBER", "OSCAR", "PAPA", "QUEBEC", 
		"ROMEO", "SIERRA", "TANGO", "UNIFORM", "VICTOR", "WHISKY", "X-RAY", "YANKEE", "ZULU"]


# Preberi iz stdin
l = input("Vpiši zakodirano sporočilo: ")
# Razdeli po presledkih
besede = l.split(" ")

rezultat = ""
for beseda in besede:
	# Preveri vse kodne besede
	for koda in kode:
		# Če se ujemata
		if close_enough(koda, beseda):
			# Dodaj črko k rezultatu
			rezultat += koda[0]
			break

# Izpiši dekodirano besedilo
print(rezultat)

# Pravilen rezultat: SIERRA RODEO ERHO CHARLIF MOVEMBER OSQAR -> SRECNO ✔