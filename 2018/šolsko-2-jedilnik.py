primer_jedilnika = ["svinjski zrezek v omaki", "sirov kanelon", "ocvrt oslič", "svinjski zrezek v omaki", "ocvrt oslič", "sirov burek", "sirov kanelon", "ocvrt oslič", "sirov kanelon", "sirov kanelon"]

def KdajSpet(jedilnik: list) -> list:
	rezultat = []
	for d, jed in enumerate(jedilnik):
		for i in range(1, len(jedilnik) + 1):
			if jedilnik[(d+i) % len(jedilnik)] == jed:
				rezultat.append(i)
				break
	return rezultat

print(KdajSpet(primer_jedilnika))  # Pravilen rezultat: [3, 5, 2, 7, 3, 10, 2, 5, 1, 2] ✔
print(KdajSpet(["burek", "jogurt", "burek"]))  # Pravilen rezultat: [2, 3, 1] ✔
