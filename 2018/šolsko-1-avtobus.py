n = 5
# Marprom linija 21. Kak žalostno je, da to vem na pamet?
p = ["TPC City", "Glavni trg - Vetrinjska", "Magdalenski park", "Ljubljanska - 2. gimnazija", "Ljubljanska - Focheva"]
d = [5, 17, 8, 9, 12]

def avtobusi():
	ura = 0
	for i in range(0, n):
		postaja = p[i]
		do_sem = d[i]
		čas_pisanja = len(postaja) + 1

		if čas_pisanja < ura + do_sem:
			return postaja
		ura += do_sem
	return "doma"

print(avtobusi())
