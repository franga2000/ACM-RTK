
def Kosi(m: int, b: int = 0):
	načini = 0
	for i in range(1, 4):
		n = b + i
		if n < m:
			načini += Kosi(m, n)
		elif n == m:
			načini += 1
		else:
			break
	return načini

print(Kosi(3))  # Pravilen rezultat: 4 ✔
