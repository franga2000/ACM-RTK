"""
Dokler števec ni do konca resetiran, se bo pomikal navzdol (hitrost bo ignoriral).
Po tem, ko je resetiran, bo njegova pozicija shranjena v spremenljivki kazalec.
Ko program dobi meritev hitrosti jo primerja s stanjem kazalca in ga premakne tako, da ji bo bližje.
"""


# Začne na -250, da resetira kazalec
reset = 250
# Trenutna pozicija kazalca (po resetu)
kazalec = 0


def Premik(hitrost: int) -> int:
	global reset, kazalec
	# Najprej moramo kazalec resetirati
	if reset > 0:
		reset -= 1
		return -1
	
	# Če je hitrost večja od kazalca, kazalec premakni gor
	if hitrost > kazalec and kazalec <= 250:
		kazalec += 1
		return +1
	# Če je hitrost manjša od kazalca, kazalec premakni dol
	elif hitrost < kazalec and kazalec >= 0:
		kazalec -= 1
		return -1
	# Če ni za naredit nič, ne naredi nič
	else: 
		return 0
