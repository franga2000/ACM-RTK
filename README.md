# Rešitve nalog s Tekmovanja ACM

Moje rešitve nalog s [Tekmovanja ACM iz računalništva in informatike](http://rtk.ijs.si/index.html).  
Navodila vseh nalog najdete na [strani tekmovanja](http://rtk.ijs.si/prejsnja.html).

## Struktura repozitorija
```
.
└── leto
    ├── nivo-naloga-naslov_naloge.tip_rešitve
    └── vhodna_datoteka
```

 - `nivo`:
	 - `šolsko` za šolsko tekmovanje
	 - `1`, `2` ali `3` za skupine državnega tekmovanja
 - `naloga`: Številka naloge
 - `naslov_naloge`: Približen naslov naloge
 - `tip_rešitve`:
	 - Standardna končnica za programe (npr. `.py`, `.java`)
	 - `.txt` za opise postopkov
