avtobusi = [(0, 40), (1, 50), (2, 30), (3, 30), (4, 30), (5, 35), (6, 5)]
filmi = [(1, 30), (2, 10), (1, 40), (0, 50)]

# Pretvori vse čase v minute
avtobusi = [t[0]*60 + t[1] for t in avtobusi]
filmi = [t[0]*60 + t[1] for t in filmi]

# Inicializacija rešitve
optim_razlika = float("+infinity")
optim_film = 0

čas = 0
for film, dolžina in enumerate(filmi, start=1):
	čas += dolžina
	for odhod in avtobusi:
		razlika = odhod - čas
		if razlika < 0:
			continue
		if razlika < optim_razlika:
			optim_razlika = razlika
			optim_film = film

# Izpis rešitve
print("Pogleda naj %d filme" % optim_film)
print("Na bus bo čakal %d minut" % optim_razlika)
