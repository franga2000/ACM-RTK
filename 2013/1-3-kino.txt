Za začetek bi vse časovne podatke pretvoril v minute.

Inicializiraj rešitev: film := 0, čas čakanja := +neskončno
čas := 0
Za vsak film:
	čas += dolžina filma
	Za vsaki avtobus:
		razlika := prihod avtobusa - čas
		Če je razlika negativna:
			Pojdi na naslednji avtobus
		Če je razlika manjša od trenutne rešitve:
			Posodobi rešitev: film := št. filma, čas čakanja := čas

Izpis rešitve
